import React from 'react'
import PropTypes from 'prop-types'
import {connect } from 'react-redux';
import Slid from '../../common/slid/containers/Slid.js';

import {Acts} from '../../../actions/index.js';

class EditSlidPanel extends React.Component {

  constructor(props) {
    super(props);
  }
  static propTypes = {
    slid: PropTypes.object
  };

  render () {
    var render = null;
    let content = this.props.contentMap[this.props.slid.contentId];
    let propsSlid = {...this.props.slid, "content": content};
    render = <Slid {...propsSlid} opts={{"displayMode": "FULL_MNG"}} updateSlid={this.props.updateSlid}/>;
    return (
      <div className="edit-slid-panel">
        {render}
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    "slid": state.selectedReducer.slid,
    "contentMap": state.updateModelReducer.contentMap
  }
};

const mapDispatchToProps = (dispatch) => {
    return {"updateSlid": (slid) => dispatch(Acts.UPDATE_SLID(slid))};
};

export default connect(mapStateToProps, mapDispatchToProps)(EditSlidPanel);
