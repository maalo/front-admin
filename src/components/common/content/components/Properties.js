import React, { Component } from 'react';
import Visual from './Visual';

class Properties extends Component {
    constructor(props) {
        super(props);
    }

  render() {
    return (
        <div>
            <Visual
                type={this.props.type}
                src={this.props.src}
            />
            <div className="panel-footing">
                <h3 className="panel-title">ID: {this.props.id} Title: {this.props.title}</h3>
            </div>
        </div>
    );
  }
}

export default Properties;
