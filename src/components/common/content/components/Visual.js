  import React, { Component } from 'react';

  class Visual extends Component {
    render() {
      let render_visual;
      switch(this.props.type){
          case "img_url":
              render_visual=(
                  <img
                      className='imgCard'
                      src={this.props.src}
                      width="100%" height="100%"
                  />
                  );
          break;
          case "video":
                  render_visual=(
                  <object  width="100%" height="100%"
                          data={this.props.src}>
                  </object>
                  );
          break;
      }

      return (
          <div className="panel-body">
              {render_visual}
          </div>
      );
    }
  }

  export default Visual;
