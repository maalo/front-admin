import React from 'react';
import Visual from '../components/Visual';
import Properties from '../components/Properties';

export default class Content extends React.Component {

  render() {
    let render_visual = "";
    if (this.props.onlyContent) {
      render_visual = (
        <Visual
          type={this.props.type}
          src={this.props.src}/>
      );
    } else {
      render_visual = (
        <Properties
          title={this.props.title}
          id={this.props.id}
          src={this.props.src}
          type={this.props.type}/>
      );
    }

    return (
      <div className="panel panel-default">
        {render_visual}
      </div>
    );
  }
}
