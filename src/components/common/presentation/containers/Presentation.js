import React, { Component } from 'react';
import PropTypes from 'prop-types'
import './presentation.css';
import Slid from '../../slid/containers/Slid.js'

//import {updateSelectedSlid, addNewSlid, removeCurrentSlid} from '../../../../actions'
import {Acts} from '../../../../actions/index.js'
import SlidService from '../../../../services/SlidService.js'
import {connect} from 'react-redux';

class Presentation extends Component {


  constructor(props){
    super(props);
  }
  static propTypes = {
    id: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    slidArray: PropTypes.array,
    updateSelectedSlid: PropTypes.func
  };

  static defaultProps = {
    slidArray: []
  };

  handleAddClick(){
    this.props.addNewSlid(SlidService.getNewSlide());
  }
  handleSaveClick(){
  }

  handleChangeDescription(){

  }
  render() {
      let slides = this.props.slidArray.map((slide) => {
      let content = this.props.contentMap[slide.contentId];
      let propsSlid = {...slide, "content": content};
      return <div key={slide.id} onClick={() => this.props.updateSelectedSlid(slide)}>
          <Slid {...propsSlid} key={slide.id} opts={{"displayMode": "SHORT"}} />
        </div>
      });
      return (
        <div className="presentation">
          <button onClick={this.handleAddClick.bind(this)} type="button" className="btn btn-link"><span  className="glyphicon glyphicon-plus"></span></button>
          <button onClick={this.handleSaveClick.bind(this)} type="button" className="btn btn-link"><span  className="glyphicon glyphicon-save"></span></button>

          <div className="form-group">
            <label htmlFor="currentPresTitle">Title
            </label>
            <input type="text" className="form-control" id="currentPresTitle" onChange={this.props.handleChangeTitle} value={this.props.title}/>
            <label htmlFor="currentPresDescription">Description</label>
            <textarea rows="5" type="text" className="form-control" id="currentPresDescription" onChange={this.handleChangeDescription} value={this.props.description}></textarea>
          </div>
          {slides}
        </div>
      );
  }
}

const mapDispatchToProps = (dispatch) => {
//  return bindActionCreators({"updateSlid": updateSlid}, dispatch);
    return({
        "updateSelectedSlid": (slid) => dispatch(Acts.UPDATE_SELECTED_SLID(slid)),
        "addNewSlid": (slid) => dispatch(Acts.ADD_NEW_SLID(slid))
    });
}

const mapStateToProps = (state) => {
  return {
    "slidArray": state.updateModelReducer.presentation.slidArray,
    "contentMap": state.updateModelReducer.contentMap
  }
};

export default connect(mapStateToProps,mapDispatchToProps)(Presentation);
