import React from 'react';
import PropTypes from 'prop-types';
import './slid.css';
import EditMetaSlid from '../components/EditMetaSlid.js';
import Content from '../../content/containers/Content.js';

import {connect} from 'react-redux';
import {Acts} from '../../../../actions';

class Slid extends React.Component {

  constructor(props){
    super(props);
  }

  static propTypes = {
    //data
    id: PropTypes.string,
    title: PropTypes.string,
    txt: PropTypes.string,
    content: PropTypes.object.isRequired,
    //opts
    opts: PropTypes.object, //SHORT or FULL_MNG
    //funcs
    updateSlid: PropTypes.func,
    setSelectedSlid: PropTypes.func
  };

  static defaultProps = {
    opts: {}
  };

  handleChangeTitle(ev){
    this.props.updateSlid({...this.props, title:ev.target.value});
  }
  handleChangeTxt(ev){
    this.props.updateSlid({...this.props, txt:ev.target.value});
  }

  handleRemoveClick(){
    this.props.removeSlid(this.props.id);
  }

  render() {
    if(this.props.content == undefined){return null;}
    let to_render;
    switch (this.props.opts.displayMode) {
      case 'SHORT':
        to_render = [< Content key={this.props.content.id} {...this.props.content} onlyContent = {true} />];
        break;
      case 'FULL_MNG':
        to_render = [
          < EditMetaSlid key="0" title={this.props.title} txt={this.props.txt} handleChangeTitle={this.handleChangeTitle.bind(this)} handleChangeTxt={this.handleChangeTxt.bind(this)}/>,
        < Content key={this.props.content.id} {...this.props.content} onlyContent = {true} />
      ];
        break;
      break;
    }
    return (
      <div className="slid">
        {this.props.id}
        <button onClick={this.handleRemoveClick.bind(this)} type="button" className="btn btn-link"><span  className="glyphicon glyphicon-minus"></span></button>
        {to_render}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
//  return bindActionCreators({"updateSlid": updateSlid}, dispatch);
    return{
        "setSelectedSlid": (slid) => dispatch(Acts.UPDATE_SELECTED_SLID(slid)),
        "removeSlid": (id) => dispatch(Acts.REMOVE_SLID(id))
    };
}

export default connect(null,mapDispatchToProps)(Slid);
