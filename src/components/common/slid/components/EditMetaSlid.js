import React, {Component} from 'react';
import './editMetaSlid.css';
import PropTypes from 'prop-types'

class EditMetaSlid extends Component {

  static propTypes = {
    handleChangeTitle: PropTypes.func,
    handleChangeTxt: PropTypes.func,
    txt: PropTypes.string,
    title: PropTypes.string
  };
  render() {
    return (
      <div className="form-group">
        <label htmlFor="currentSlideTitle">Title
        </label>
        <input type="text" className="form-control" id="currentSlideTitle" onChange={this.props.handleChangeTitle} value={this.props.title}/>
        <label htmlFor="currentSlideText">Text</label>
        <textarea rows="5" type="text" className="form-control" id="currentSlideText" onChange={this.props.handleChangeTxt} value={this.props.txt}></textarea>
      </div>
    );
  }
}

export default EditMetaSlid;
