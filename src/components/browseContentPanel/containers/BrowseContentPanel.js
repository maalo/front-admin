import React from 'react';
import './browseContentPanel.css';
import Content from '../../common/content/containers/Content.js'

export default class BrowseContentPanel extends React.Component {
    render() {
        let contents = Object.values(this.props.contentMap)
        .map((content) => <Content key={content.id} id={content.id} title={content.title}  type={content.type} src={content.src} onlyContent={false}/>);
        return (
            <div className='browse-content-panel container-fluid height-100'>
              {contents}
            </div>
        );
    }
}
