import React from 'react';
import './main.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as contentMapTmp from '../../source/contentMap.json';
import BrowseContentPanel from '../browseContentPanel/containers/BrowseContentPanel.js';
import EditSlidPanel from '../editSlidPanel/containers/EditSlidPanel.js';

import Presentation from '../common/presentation/containers/Presentation.js';
import {connect } from 'react-redux';
import PropTypes from 'prop-types';
import PresentationService from '../../services/PresentationService.js';

import {Acts} from '../../actions/index.js'


class Main extends React.Component {

  static defaultProps = {
    "contentMap": {},
    "presentation": {"description": "youuuuu"}
  };

  static propTypes = {
    "updateContentMap": PropTypes.func,
    "updatePresentation": PropTypes.func
  };

  constructor(props) {
    super(props);
    this.props.updatePresentation(PresentationService.getNewPresentation());
    this.props.updateContentMap(contentMapTmp);
  }


  render() {
    let sidePanel = !!this.props.currentSlid ? <EditSlidPanel slid={this.props.currentSlid}></EditSlidPanel> : "no";
    return (
        <div className='main container-fluid height-100'>
          <div className="row height-100">
            <div className='col-md-3 col-lg-3 height-100 vertical-scroll'>
              <Presentation {...this.props.presentation} />
            </div>
            <div className='col-md-6 col-lg-6 height-100'>
              {sidePanel}
            </div>
            <div className='col-md-3 col-lg-3 height-100'>
              <BrowseContentPanel contentMap={this.props.contentMap}/>
            </div>
          </div>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    "contentMap": state.updateModelReducer.contentMap,
    "presentation": state.updateModelReducer.presentation,
    "currentSlid": state.selectedReducer.slid
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    "updatePresentation": (pres) => dispatch(Acts.UPDATE_PRESENTATION(pres)),
    "updateContentMap": (contentMap) => dispatch(Acts.UPDATE_CONTENT_MAP(contentMap))
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(Main);
