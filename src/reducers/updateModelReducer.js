import {Csts} from '../actions/index.js'

const updateModelReducer = (state = {
  presentation: {},
  contentMap: {}
}, action) => {
  console.log(action);
  switch (action.type) {
    case Csts.UPDATE_PRESENTATION:
      return {...state, presentation: action.presentation};
      //TO DO
    case Csts.UPDATE_CONTENT_MAP:
      return {...state, contentMap: action.contentMap};
      //TO DO
    case Csts.ADD_NEW_SLID:
      return {...state, presentation: {"slidArray": [...state.presentation.slidArray, action.slid]}}
    case Csts.UPDATE_SLID:
      var pres = {
        ...state.presentation,
        "slidArray": state.presentation.slidArray.map((slid) => {return slid.id == action.slid.id ? action.slid : slid})
      };
      console.log(pres);
      return {...state, presentation: pres};
    case Csts.REMOVE_SLID:
      var pres = {
        ...state.presentation,
        "slidArray": state.presentation.slidArray.filter((slid) => {return slid.id != action.id})
      };
      return {...state, presentation: pres};
      //TO DO
    default:
      return state;
  }
}

export default updateModelReducer;
