import { combineReducers } from 'redux'
import selectedReducer from './selectedReducer'
import updateModelReducer from './updateModelReducer'

export default combineReducers({
  selectedReducer,
  updateModelReducer
});
