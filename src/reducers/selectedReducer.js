import {Csts} from '../actions/index.js'


console.log(Csts.UPDATE_SELECTED_SLID);
const selectedReducer = (state = {
  "slid": {}
}, action) => {
  switch (action.type) {
    case Csts.UPDATE_SELECTED_SLID:
      return {...state, slid: action.slid};
    case Csts.UPDATE_SLID:
      return {...state, slid: action.slid};
    default:
      return state;
  }
}
export default selectedReducer;
