import EasyActions from 'redux-easy-actions';

const { Actions, Constants } = EasyActions({
   UPDATE_SELECTED_SLID(type, slid){
       return {type, slid}
   },
   UPDATE_CONTENT_MAP(type, contentMap){
       return {type, contentMap}
   },
   UPDATE_SLID(type, slid){
       return {type, slid}
   },
   UPDATE_PRESENTATION(type, presentation){
       return {type, presentation}
   },
   ADD_NEW_SLID(type, slid){
       return {type, slid}
   },
   REMOVE_SLID(type, id){
     return {type, id}
   }
})

export { Actions as Acts }
export { Constants as Csts }
